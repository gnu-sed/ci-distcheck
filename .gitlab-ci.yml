# Reference documentation for this file:
# https://docs.gitlab.com/ee/ci/yaml/
#
# Syntax of multiline strings in YAML: https://yaml-multiline.info/

variables:
  # Overrides of https://docs.gitlab.com/ee/ci/variables/predefined_variables.html
  GET_SOURCES_ATTEMPTS: "3"
  # Prerequisite packages (listed in the DEPENDENCIES and HACKING files)
  # for each build environment.
  # Note! After changing such a variable, you have to regenerate the corresponding
  # build environment.
  PREREQ_ubuntu2004:
    git
    wget python3 automake autoconf m4 autopoint help2man texinfo xz-utils
    file
    make gcc gettext
    texlive
  # Optional packages (listed in the DEPENDENCIES and HACKING files)
  # for each build environment.
  EXTRA_ubuntu2004:
    libacl1-dev libattr1-dev

# Verify that the tests pass.
# Simple implementation that always uses a clean checkout.
# Time: 8 min.
check-clean:
  when: manual
  #
  # Start with an image from the docker hub (https://hub.docker.com/).
  image: ubuntu:20.04
  # It would also be possible to start with an image that contains a specific
  # GCC version, see https://hub.docker.com/_/gcc
  #image: gcc:9.5.0
  #image: gcc:10.4.0
  #image: gcc:11.3.0
  #image: gcc:12.2.0
  #
  before_script:
    - uname -a
    - pwd
    - env | LC_ALL=C sort
    # Install packages.
    # The environment variable DEBIAN_FRONTEND avoids hanging in 'Configuring tzdata'.
    - apt update && DEBIAN_FRONTEND=noninteractive apt install -y $PREREQ_ubuntu2004
    # Fetch sources (uses package 'git').
    - basedir=`pwd`
    - mkdir -p sources
    - cd sources
    - git clone https://git.savannah.gnu.org/git/sed.git
    - git clone --depth 1 https://git.savannah.gnu.org/git/gnulib.git
    - GNULIB_SRCDIR=`pwd`/gnulib; export GNULIB_SRCDIR
    - cd sed
    # Force use of the newest gnulib.
    - rm -f .gitmodules
    # Disable a test that fails when run as root.
    - sed -i -e '/panic-tests/d' testsuite/local.mk
    # Fetch extra files and generate files (uses packages wget, python3, automake, autoconf, m4,
    # help2man, texinfo, xz-utils).
    - |
      { $GNULIB_SRCDIR/build-aux/git-version-gen .tarball-version | sed -e 's/-dirty$//'; echo -n '-'; date --utc --iso-8601; } > .tarball-version
    - ./bootstrap --no-git --gnulib-srcdir="$GNULIB_SRCDIR"
  script:
    - cd $basedir
    # Build sed.
    - rm -rf build
    - mkdir -p build/sed
    - cd build/sed
    # Configure (uses package 'file').
    - ../../sources/sed/configure --config-cache CPPFLAGS="-Wall" > log1 2>&1; rc=$?; cat log1; test $rc = 0 || exit 1
    # Build (uses packages make, gcc, ...).
    - make > log2 2>&1; rc=$?; cat log2; test $rc = 0 || exit 1
    # Run the tests.
    - ( make check 2>&1 || { echo '====================== sed.log ======================'; cat testsuite/sed.log; }) | tee log3; if grep '^FAIL:' testsuite/sed.log >/dev/null; then exit 1; fi
    # Check that tarballs are correct.
    - make distcheck > log4 2>&1; rc=$?; cat log4; test $rc = 0 || exit 1
    - mv sed-*.tar.gz ../../
    # Build again, this time with optional packages installed.
    - cd $basedir
    - apt update && DEBIAN_FRONTEND=noninteractive apt install -y $EXTRA_ubuntu2004
    - rm -rf build-full
    - mkdir -p build-full/sed
    - cd build-full/sed
    - ../../sources/sed/configure --config-cache CPPFLAGS="-Wall" > log1 2>&1; rc=$?; cat log1; test $rc = 0 || exit 1
    - make > log2 2>&1; rc=$?; cat log2; test $rc = 0 || exit 1
    - ( make check 2>&1 || { echo '====================== sed.log ======================'; cat testsuite/sed.log; }) | tee log3; if grep '^FAIL:' testsuite/sed.log >/dev/null; then exit 1; fi
    - make install > log5 2>&1; rc=$?; cat log5; test $rc = 0 || exit 1
    - cd ../..
    - tar cf sed-snapshot.tar sed-*.tar.gz
  #
  artifacts:
    when: always
    paths:
      - build/sed/config.cache
      - build/sed/config.log
      - build/sed/config.status
      - build/sed/log[1234]
      - build-full/sed/config.cache
      - build-full/sed/config.log
      - build-full/sed/config.status
      - build-full/sed/log[1235]
      - sed-*.tar
    expire_in: 8 days


# Integration of Gitlab with Docker:
# https://docs.gitlab.com/ee/ci/docker/using_docker_build.html

# Build the buildenv named 'ubuntu2004'.
generate-ubuntu2004:
  variables:
    BUILDENV: ubuntu2004
  #
  when: manual
  #
  # Use the docker-in-docker approach.
  services:
    - docker:dind
  image: docker:stable
  script:
    - uname -a
    - pwd
    - env | LC_ALL=C sort
    # Doc: https://docs.gitlab.com/ee/user/project/new_ci_build_permissions_model.html
    # https://stackoverflow.com/questions/37468084/what-is-the-special-gitlab-ci-token-user
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - prereqvar=PREREQ_$BUILDENV; eval packages='$'$prereqvar; docker build --no-cache -t $CI_REGISTRY/$CI_PROJECT_PATH:$BUILDENV --build-arg "packages=$packages" .gitlab-ci-images/$BUILDENV
    # Authenticate for pushing.
    # See https://docs.gitlab.com/ee/ci/variables/predefined_variables.html
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker push $CI_REGISTRY/$CI_PROJECT_PATH:$BUILDENV

# Verify that the tests pass.
# Implementation with pre-built docker image.
# The build environment ubuntu2004 must have been generated first.
# Time: 7 min.
check-optimized:
  when: always
  # Don't run this job too often.
  # https://docs.gitlab.com/ee/ci/yaml/#onlyexcept-basic
  only:
    - schedules
    - web
  #
  # Start with an image that contains all the required packages.
  image: $CI_REGISTRY/$CI_PROJECT_PATH:ubuntu2004
  #
  before_script:
    - uname -a
    - pwd
    - env | LC_ALL=C sort
    # Fetch sources (uses package 'git').
    - basedir=`pwd`
    - mkdir -p sources
    - cd sources
    - git clone https://git.savannah.gnu.org/git/sed.git
    - git clone --depth 1 https://git.savannah.gnu.org/git/gnulib.git
    - GNULIB_SRCDIR=`pwd`/gnulib; export GNULIB_SRCDIR
    - cd sed
    # Force use of the newest gnulib.
    - rm -f .gitmodules
    # Disable a test that fails when run as root.
    - sed -i -e '/panic-tests/d' testsuite/local.mk
    # Fetch extra files and generate files (uses packages wget, python3, automake, autoconf, m4,
    # help2man, texinfo, xz-utils).
    - |
      { $GNULIB_SRCDIR/build-aux/git-version-gen .tarball-version | sed -e 's/-dirty$//'; date --utc --iso-8601; } > .tarball-version
    - ./bootstrap --no-git --gnulib-srcdir="$GNULIB_SRCDIR"
  script:
    - cd $basedir
    # Build sed.
    - rm -rf build
    - mkdir -p build/sed
    - cd build/sed
    # Configure (uses package 'file').
    - ../../sources/sed/configure --config-cache CPPFLAGS="-Wall" > log1 2>&1; rc=$?; cat log1; test $rc = 0 || exit 1
    # Build (uses packages make, gcc, ...).
    - make > log2 2>&1; rc=$?; cat log2; test $rc = 0 || exit 1
    # Run the tests.
    - ( make check 2>&1 || { echo '====================== sed.log ======================'; cat testsuite/sed.log; }) | tee log3; if grep '^FAIL:' testsuite/sed.log >/dev/null; then exit 1; fi
    # Check that tarballs are correct.
    - make distcheck > log4 2>&1; rc=$?; cat log4; test $rc = 0 || exit 1
    - mv sed-*.tar.gz ../../
    # Build again, this time with optional packages installed.
    - cd $basedir
    - apt update && DEBIAN_FRONTEND=noninteractive apt install -y $EXTRA_ubuntu2004
    - rm -rf build-full
    - mkdir -p build-full/sed
    - cd build-full/sed
    - ../../sources/sed/configure --config-cache CPPFLAGS="-Wall" > log1 2>&1; rc=$?; cat log1; test $rc = 0 || exit 1
    - make > log2 2>&1; rc=$?; cat log2; test $rc = 0 || exit 1
    - ( make check 2>&1 || { echo '====================== sed.log ======================'; cat testsuite/sed.log; }) | tee log3; if grep '^FAIL:' testsuite/sed.log >/dev/null; then exit 1; fi
    - make install > log5 2>&1; rc=$?; cat log5; test $rc = 0 || exit 1
    - cd ../..
    - tar cf sed-snapshot.tar sed-*.tar.gz
  #
  artifacts:
    when: always
    paths:
      - build/sed/config.cache
      - build/sed/config.log
      - build/sed/config.status
      - build/sed/log[1234]
      - build-full/sed/config.cache
      - build-full/sed/config.log
      - build-full/sed/config.status
      - build-full/sed/log[1235]
      - sed-*.tar
    expire_in: 8 days
