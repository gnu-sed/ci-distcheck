# ci-distcheck

Creates tarballs and performs checks.

# The build environments

This project also contains jobs that create build environments for optimized
execution of ci-distcheck.

Each build environment is in fact a build image (as a docker image) that
contain the *static* parts of a build done by ci-distcheck/.gitlab-ci.yml,
that is, the parts that usually don't change within a couple of months.

1. It is described through a Dockerfile in a subdirectory of this project,
   and a couple of lines in the .gitlab-ci.yml of this project.

2. Executing the job through the Gitlab UI:
   [CI/CD > Jobs](https://gitlab.com/gnu-sed/ci-distcheck/-/jobs)
   creates the corresponding docker image.

3. Each build image is then visible in the Gitlab UI:
   [Packages > Container Registry](https://gitlab.com/gnu-sed/ci-distcheck/container_registry).

# The created tarball

The newest created (snapshot) tarball is available through the Gitlab UI
or directly for download: [CI/CD > Jobs > Finished (check-optimized) > Job artifacts > Browse > sed-snapshot.tar](https://gitlab.com/gnu-sed/ci-distcheck/-/jobs/artifacts/main/raw/sed-snapshot.tar?job=check-optimized).
